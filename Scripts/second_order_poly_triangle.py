
m = 50  # The number of cells we want to divide the domain into
x = np.linspace(0, 1, m + 1)
x_mid = 0.5 * (x[0:-1] + x[1:])
c = 2     # Speed of the wave

# Let's also specify the initial condition
u = np.empty((m))
for i in range(m):
    if (x_mid[i] < 0.3):
        u[i] = 0
    elif (x_mid[i] <= 0.5):
        u[i] = (1/0.2) * (x_mid[i] - 0.3)
    elif (x_mid[i] <= 0.7):
        u[i] = (1/0.2) * (0.7 - x_mid[i])
    else:
        u[i] = 0
plt.plot(x_mid, u, 'k', label='Initial Condition')
#print(u)

# Interpolating u to the cell boundaries
u_intp = np.empty((m+1))
u_intp[0] = u[0]
u_intp[1:-1] = 0.5 * (u[0:-1] + u[1:])
u_intp[-1] = u[-1]

a = np.empty((3, m))
a[0, :] = u_intp[:-1]
a[1, :] = u[:]
a[2, :] = u_intp[1:]
a_new = a.copy()

t_initial = 0
t_final = 0.1
delta_x = x[1] - x[0]
delta_t = delta_x / (c*m)

xL = x[0]
xR = x[1]
xM = x_mid[0]

# Calculating the mass matrix
psi0 = lambda x: 2 * (x - xM) * (x - xR) / delta_x**2
psi1 = lambda x: -4 * (x - xL) * (x - xR) / delta_x**2
psi2 = lambda x: 2 * (x - xL) * (x - xM) / delta_x**2

M = np.empty((3, 3))
M[0, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi0(x), xL, xR)[0]
M[0, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi0(x), xL, xR)[0]
M[0, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi0(x), xL, xR)[0]
M[1, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi1(x), xL, xR)[0]
M[1, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi1(x), xL, xR)[0]
M[1, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi1(x), xL, xR)[0]
M[2, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi2(x), xL, xR)[0]
M[2, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi2(x), xL, xR)[0]
M[2, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi2(x), xL, xR)[0]

S = np.empty((3, 3))
psi0_prime = lambda x: 2 * ((x - xM) + (x - xR)) / delta_x**2
psi1_prime = lambda x: -4 * ((x - xL) + (x - xR)) / delta_x**2
psi2_prime = lambda x: 2 * ((x - xL) + (x - xM)) / delta_x**2


S[0, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi0_prime(x), xL, xR)[0]
S[0, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi0_prime(x), xL, xR)[0]
S[0, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi0_prime(x), xL, xR)[0]
S[1, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi1_prime(x), xL, xR)[0]
S[1, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi1_prime(x), xL, xR)[0]
S[1, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi1_prime(x), xL, xR)[0]
S[2, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi2_prime(x), xL, xR)[0]
S[2, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi2_prime(x), xL, xR)[0]
S[2, 2] = scipy.integrate.quad(lambda x: psi2(x)*psi2_prime(x), xL, xR)[0]

t = t_initial
while t < t_final:
    for i in range(m):
        f_hat = np.empty((3, 1))
        f_hat[0, 0] = -c * a[2, i-1]
        f_hat[1, 0] = 0
        f_hat[2, 0] = c * a[2, i]
        
        a_prime = np.empty((3, 1))
        a_prime[:, 0] = np.dot(np.linalg.inv(M), c * np.dot(S, a[:, i]) - f_hat[:, 0])
        
#         if m%10 == 0:
#             print(a_prime)
        a_new[:, i] = a[:, i] + a_prime[:, 0] * delta_t
    
    a = a_new.copy()
    t += delta_t

for i in range(m):
    xL = x[i]
    xR = x[i+1]
    xM = x_mid[i]
    u[i] = a[0, i] * psi0(x_mid[i]) + a[1, i] * psi1(x_mid[i]) + a[2, i] * psi2(x_mid[i])
plt.plot(x_mid, u, 'bx', label='Numerical Solution')
    
u_analytical = np.empty((m))
for i in range(m):
    if (x_mid[i] < 0.5):
        u_analytical[i] = 0
    elif (x_mid[i] <= 0.7):
        u_analytical[i] = (1/0.2) * (x_mid[i] - 0.5)
    elif (x_mid[i] <= 0.9):
        u_analytical[i] = (1/0.2) * (0.9 - x_mid[i])
    else:
        u_analytical[i] = 0
plt.plot(x_mid, u_analytical, 'b', label='Analytical Solution')
plt.legend()
plt.title("Results with 2nd degree polynomial")
plt.xlabel("Domain")
#plt.savefig('second_order_poly_50.jpg', dpi=300)
plt.show()
