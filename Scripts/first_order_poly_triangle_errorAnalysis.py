
import numpy as np
#from sympy import *
import scipy.integrate
import matplotlib.pyplot as plt
import numpy

# Compute the error as a function of delta_x
delta_x = []
error = []

for m in range(20, 100, 20):
    x = np.linspace(0, 1, m + 1)
    x_mid = 0.5 * (x[0:-1] + x[1:])
    c = 2     # Speed of the wave

    # Let's also specify the initial condition
    u = np.empty((m))
    for i in range(m):
        if (x_mid[i] < 0.3):
            u[i] = 0
        elif (x_mid[i] <= 0.5):
            u[i] = (1/0.2) * (x_mid[i] - 0.3)
        elif (x_mid[i] <= 0.7):
            u[i] = (1/0.2) * (0.7 - x_mid[i])
        else:
            u[i] = 0
    #plt.plot(x_mid, u, 'k', label='Initial Condition')
    #print(u)

    # Interpolating u to the cell boundaries
    u_intp = np.empty((m+1))
    u_intp[0] = u[0]
    u_intp[1:-1] = 0.5 * (u[0:-1] + u[1:])
    u_intp[-1] = u[-1]

    a = np.empty((2, m))
    a[0, :] = u_intp[:-1]
    a[1, :] = u_intp[1:]
    a_new = a.copy()

    t_initial = 0
    t_final = 0.1
    del_x = x[1] - x[0]
    CFL = 0.5
    delta_t = CFL * del_x / (c*m)
    delta_x.append(del_x)

    xL = x[0]
    xR = x[1]

    # Calculating the mass and stiffness matrix
    psi0 = lambda x: (x - xR) / (xL - xR)
    psi1 = lambda x: (x - xL) / (xR - xL)

    M = np.empty((2, 2))
    M[0, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi0(x), xL, xR)[0]
    M[0, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi0(x), xL, xR)[0]
    M[1, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi1(x), xL, xR)[0]
    M[1, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi1(x), xL, xR)[0]

    S = np.empty((2, 2))
    psi0_prime = 1 / (xL - xR)
    psi1_prime = 1 / (xR - xL)


    S[0, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi0_prime, xL, xR)[0]
    S[0, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi0_prime, xL, xR)[0]
    S[1, 0] = scipy.integrate.quad(lambda x: psi0(x)*psi1_prime, xL, xR)[0]
    S[1, 1] = scipy.integrate.quad(lambda x: psi1(x)*psi1_prime, xL, xR)[0]

    t = t_initial
    while t < t_final:
        for i in range(m):
            f_hat = np.empty((2, 1))
            f_hat[0, 0] = -c * a[1, i-1]
            f_hat[1, 0] = c * a[1, i]

            a_prime = np.empty((2, 1))
            a_prime[:, 0] = np.dot(np.linalg.inv(M), (c * np.dot(S, a[:, i]) - f_hat[:, 0]))

            a_new[:, i] = a[:, i] + a_prime[:, 0] * delta_t

        a = a_new.copy()
        t += delta_t

    for i in range(m):
        xL = x[i]
        xR = x[i+1]
        u[i] = a[0, i] * psi0(x_mid[i]) + a[1, i] * psi1(x_mid[i])

    u_analytical = np.empty((m))
    for i in range(m):
        if (x_mid[i] < 0.5):
            u_analytical[i] = 0
        elif (x_mid[i] <= 0.7):
            u_analytical[i] = (1/0.2) * (x_mid[i] - 0.5)
        elif (x_mid[i] <= 0.9):
            u_analytical[i] = (1/0.2) * (0.9 - x_mid[i])
        else:
            u_analytical[i] = 0
    
    error.append(numpy.linalg.norm(numpy.abs(del_x*(u_analytical - u)), ord=numpy.infty))
    
error = numpy.array(error)
delta_x = numpy.array(delta_x)
    
fig = plt.figure()
axes = fig.add_subplot(1, 1, 1)

axes.loglog(delta_x, error, 'ko', label="Approx. Derivative")

order_C = lambda delta_x, error, order: numpy.exp(numpy.log(error) - order * numpy.log(delta_x))
axes.loglog(delta_x, order_C(delta_x[0], error[0], 1.0) * delta_x**1.0, 'r--', label="1st Order")
axes.loglog(delta_x, order_C(delta_x[0], error[0], 2.0) * delta_x**2.0, 'b--', label="2nd Order")
axes.legend(loc=4)
axes.set_title("Convergence of 2nd Order")
axes.set_xlabel("$\Delta x$")
axes.set_ylabel("U_analytical - U_numerical")

#plt.savefig('first_order_poly_convergence.jpg', dpi=300)
plt.show()
