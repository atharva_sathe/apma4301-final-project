import numpy as np
#from sympy import *
import scipy.integrate
import matplotlib.pyplot as plt
import numpy

# Compute the error as a function of delta_x
delta_x = []
error = []

for m in range(20, 200, 20):
    x = np.linspace(0, 1, m + 1)   # Cell edges
    x_mid = 0.5 * (x[0:-1] + x[1:])   # Cell-center coordinates
    c = 2     # Speed of the wave

    # Specifying the initial condition
    # Initial condition is a simple rectangular wave with value 1 from 0.3 to 0.7 and 0 elsewhere
    u = np.empty((m))
    for i in range(m):
        if (x_mid[i] < 0.3):
            u[i] = 0
        elif (x_mid[i] <= 0.5):
            u[i] = (1/0.2) * (x_mid[i] - 0.3)
        elif (x_mid[i] <= 0.7):
            u[i] = (1/0.2) * (0.7 - x_mid[i])
        else:
            u[i] = 0
    #print(u)

    # Defining coefficients of the basis functions for linear approximation of the solution
    # Since in this case, we have a zero degree polynomial, we'll have only one coefficient for that constant value
    a = u[:]
    a_new = a.copy()

    # Specifying initial time, final time and the increment
    t_initial = 0   
    t_final = 0.1
    del_x = x[1] - x[0]
    delta_t = 0.5*(del_x) / c    # CFL is set to 0.5
    delta_x.append(del_x)

    # Calculating the Mass and Stiffness Matrices
    # Since we have a zero degree polynomial, the Mass and Stiffness Matrices will just be a single number
    M = del_x  # Mass Matrix
    S = 0  # Stiffness Matrix

    # Iterating the initial value
    t = t_initial
    while t < t_final:
        for i in range(m):
            f_hat = -c * a[i-1] + c * a[i]    # Upwind flux scheme
            #a_prime = np.dot(np.linalg.inv(M), (c * np.dot(S, a[i]) - f_hat))
            a_prime = (1 / M) * (c * S * a[i] - f_hat)  # Semi-discrete system for DG method

            a_new[i] = a[i] + a_prime * delta_t    # Forward Euler

        a = a_new.copy()
        t += delta_t

    # Reconstructing the solution
    u = a[:]

    u_analytical = np.empty((m))
    for i in range(m):
        if (x_mid[i] < 0.5):
            u_analytical[i] = 0
        elif (x_mid[i] <= 0.7):
            u_analytical[i] = (1/0.2) * (x_mid[i] - 0.5)
        elif (x_mid[i] <= 0.9):
            u_analytical[i] = (1/0.2) * (0.9 - x_mid[i])
        else:
            u_analytical[i] = 0
    
    error.append(numpy.linalg.norm(numpy.abs((u_analytical - u)), ord=numpy.infty))
    
error = numpy.array(error)
delta_x = numpy.array(delta_x)
    
fig = plt.figure()
axes = fig.add_subplot(1, 1, 1)

axes.loglog(delta_x, error, 'ko', label="Approx. Derivative")

order_C = lambda delta_x, error, order: numpy.exp(numpy.log(error) - order * numpy.log(delta_x))
axes.loglog(delta_x, order_C(delta_x[0], error[0], 1.0) * delta_x**1.0, 'r--', label="1st Order")
axes.loglog(delta_x, order_C(delta_x[0], error[0], 2.0) * delta_x**2.0, 'b--', label="2nd Order")
axes.legend(loc=4)
axes.set_title("Convergence of 1st Order")
axes.set_xlabel("$\Delta x$")
axes.set_ylabel("U_analytical - U_numerical")

#plt.savefig('zero_order_poly_convergence.jpg', dpi=300)
plt.show()
