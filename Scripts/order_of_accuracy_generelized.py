import numpy as np
#from sympy import *
import scipy.integrate
import matplotlib.pyplot as plt
from scipy.interpolate import lagrange

# Another approach, where basis function scales like (1 - x) and x in each cell

m = 100  # The number of cells we want to divide the domain into
x = np.linspace(0, 1, m + 1)
x_mid = 0.5 * (x[0:-1] + x[1:])
c = 2     # Speed of the wave

# Initial Condition
u = np.empty((m))
for i in range(m):
    if (abs(x_mid[i] - 0.5) < 0.2):
        u[i] = 1
    else:
        u[i] = 0
plt.plot(x_mid, u)
#print(u)
##########################################
# Let's say we want nth order accuracy
p = []

n = 5
z = np.linspace(0, 1, n)
y = np.zeros(n)

for i in range(n):
    y[i] = 1
    p.append(lagrange(z, y))
    y[i] = 0

M = np.empty((n, n))
for i in range(n):
    for j in range(n):
        M[i, j] = scipy.integrate.quad(p[i]*p[j], z[0], z[-1])[0]

S = np.empty((n, n))
for i in range(n):
    for j in range(n):
        S[i, j] = scipy.integrate.quad(np.polyder(p[i])*p[j], z[0], z[-1])[0]
##########################################
t_initial = 0
t_final = 10
delta_x = x[1] - x[0]
delta_t = 0.1 * delta_x / c

a = np.empty((n, m))
for i in range(n):
    a[i, :] = u[:]

t = t_initial
while t < t_final:
    for i in range(m):
        f_hat = np.empty((n, 1))
        f_hat[0, 0] = -c * a[n-1, i-1]
        f_hat[1:n-1, 0] = 0
        f_hat[n-1, 0] = c * a[n-1, i]
        
        a_prime = np.empty((n, 1))
        a_prime[:, 0] = np.dot(np.linalg.inv(M), c * np.dot(S, a[:, i]) - f_hat[:, 0])
        a[:, i] = a[:, i] + a_prime[:, 0] * delta_t
    
    t += delta_t

u_solution = np.empty(m)
for i in range(m):
    for j in range(n):
        u_solution[i] += a[j, i] * p[j](x_mid[i])

plt.plot(x_mid, u_solution)
plt.ylim(-0.1, 1.5)
plt.title("4 degree polynomial approximation")
plt.xlabel("Domain")
#plt.savefig("fourth_order_poly_blowup.png", dpi=300)
plt.show()
