
import numpy as np
#from sympy import *
import scipy.integrate
import matplotlib.pyplot as plt
import math
import numpy

'''This code uses a constant polynomial as a basis function to approximate the solution inside each cell.
It's a very basic case in which the Mass Matrix and the Stiffness Matrix are reduced to a single number.
This case was chosen just to get started with DG methods. Periodic boundary condition is specified for this problem'''

m = 100 # The number of cells we want to divide the domain into
x = np.linspace(0, 1, m + 1)   # Cell edges
x_mid = 0.5 * (x[0:-1] + x[1:])   # Cell-center coordinates
c = 2     # Speed of the wave

# Specifying the initial condition
# Initial condition is a simple rectangular wave with value 1 from 0.3 to 0.7 and 0 elsewhere
u = np.empty((m))
for i in range(m):
    if (x_mid[i] < 0.3):
        u[i] = 0
    elif (x_mid[i] <= 0.5):
        u[i] = (1/0.2) * (x_mid[i] - 0.3)
    elif (x_mid[i] <= 0.7):
        u[i] = (1/0.2) * (0.7 - x_mid[i])
    else:
        u[i] = 0
plt.plot(x_mid, u, 'k', label='Initial Condition')  # Plotting the initial condition
#print(u)

# Defining coefficients of the basis functions for linear approximation of the solution
# Since in this case, we have a zero degree polynomial, we'll have only one coefficient for that constant value
a = u[:]
a_new = a.copy()

# Specifying initial time, final time and the increment
t_initial = 0   
t_final = 0.1
delta_x = x[1] - x[0]
CFL = 0.5
delta_t = CFL * delta_x / c    # CFL is set to 0.5


# Calculating the Mass and Stiffness Matrices
# Since we have a zero degree polynomial, the Mass and Stiffness Matrices will just be a single number
M = delta_x  # Mass Matrix
S = 0  # Stiffness Matrix

# Iterating the initial value
t = t_initial
while t < t_final:
    for i in range(m):
        f_hat = -c * a[i-1] + c * a[i]    # Upwind flux scheme
        #a_prime = np.dot(np.linalg.inv(M), (c * np.dot(S, a[i]) - f_hat))
        a_prime = (1 / M) * (c * S * a[i] - f_hat)  # Semi-discrete system for DG method

        a_new[i] = a[i] + a_prime * delta_t    # Forward Euler
    
    a = a_new.copy()
    t += delta_t

# Reconstructing the solution
u = a[:]
plt.plot(x_mid, u, 'bx', label='Numerical Solution')

u_analytical = np.empty((m))
for i in range(m):
    if (x_mid[i] < 0.5):
        u_analytical[i] = 0
    elif (x_mid[i] <= 0.7):
        u_analytical[i] = (1/0.2) * (x_mid[i] - 0.5)
    elif (x_mid[i] <= 0.9):
        u_analytical[i] = (1/0.2) * (0.9 - x_mid[i])
    else:
        u_analytical[i] = 0
plt.plot(x_mid, u_analytical, 'b', label='Analytical Solution')
plt.legend()
#plt.savefig('zero_order_poly_100.jpg', dpi=300)
plt.title("Results with 0 degree polynomial")
plt.xlabel("Domain")
plt.show()
